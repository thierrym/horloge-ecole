#!/bin/sh
fpm -n horloge-ecole \
-s dir \
-t deb \
--depends python-tk \
--name "Horloge École" \
--license GPLv3 \
--version 0.2.1 \
--architecture all \
--description "Horloge École est une application pour apprendre à lire l'heure." \
--url "https://forge.aeif.fr/ThierryM/horloge-ecole" \
--maintainer "Thierry Munoz" \
debian/horloge-ecole.desktop=/usr/share/applications/horloge-ecole.desktop \
debian/horloge-ecole=/usr/bin/horloge-ecole
